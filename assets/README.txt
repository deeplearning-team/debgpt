File: DebGPT-logo.webp
Description: DebGPT logo art generated ChatGPT with an iterative prompting process.
Copyright: 2024, Mo Zhou <lumin@debian.org>
License: CC-BY-4.0
Justification:
  According to the OpenAI's policy, the user owns the the generated image:
  https://help.openai.com/en/articles/6425277-can-i-sell-images-i-create-with-dall-e
